package tech.mastertech.itau.lancamento;

import java.time.LocalDateTime;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class LancamentoService {
	@Autowired
	private LancamentoRepository lancamentoRepository;

	@Autowired
	private CartaoClient cartaoService;

	public Lancamento criar(Lancamento lancamento) {
		Cartao cartao = cartaoService.buscar(lancamento.getNumero());

		if (cartao == null) {
			throw new ValidacaoException("cartao", "Cartão não encontrado");
		}

		if (!cartao.getAtivo()) {
			throw new ValidacaoException("cartao", "Cartão não ativo");
		}

		lancamento.setHorario(LocalDateTime.now());

		return lancamentoRepository.save(lancamento);
	}

	public Iterable<Lancamento> buscarTodasPorNumeroDoCartao(String numero) {
		return lancamentoRepository.findAllByNumero(numero);
	}
}
