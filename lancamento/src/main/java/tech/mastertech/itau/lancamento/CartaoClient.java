package tech.mastertech.itau.lancamento;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "cartao")
public interface CartaoClient {
	
	@GetMapping("/cartao/{numero}")
	Cartao buscar(@PathVariable String numero);
}
