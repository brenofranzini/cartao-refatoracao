package tech.mastertech.itau.cliente;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class ClienteService {
	@Autowired
	private ClienteRepository clienteRepository;

	private BCryptPasswordEncoder passwordEncoder;

	public Cliente criar(Cliente cliente) {
		if (cliente.getId() != 0) {
			throw new ValidacaoException("id", "Id não pode ser definido ao criar um cliente");
		}

		passwordEncoder = new BCryptPasswordEncoder();

		String senhaCriptografada = passwordEncoder.encode(cliente.getSenha());
		cliente.setSenha(senhaCriptografada);

		return clienteRepository.save(cliente);
	}

	public Optional<Cliente> buscar(int id) {
		return clienteRepository.findById(id);
	}
}
