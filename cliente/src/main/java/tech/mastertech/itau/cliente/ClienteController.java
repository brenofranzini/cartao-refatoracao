package tech.mastertech.itau.cliente;

import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

@RestController
public class ClienteController {
	@Autowired
	private ClienteService clienteService;

	@PostMapping("/cliente")
	@ResponseStatus(code = HttpStatus.CREATED)
	public Cliente criar(@Valid @RequestBody Cliente cliente) {
		return clienteService.criar(cliente);
	}

	@GetMapping("/cliente/{id}")
	public Cliente buscar(@PathVariable int id) {
		Optional<Cliente> clienteOptional = clienteService.buscar(id);

		if (clienteOptional.isPresent()) {
			return clienteOptional.get();
		}

		throw new ResponseStatusException(HttpStatus.NOT_FOUND);
	}
}
